#include <stdio.h>
#include <cstdlib>
#include <chrono>

#include "Matrix.h"

#define N 1300


/*
void printMatrix(int width, int height, int array[height][width] ){
    // Prints contents of Matrix
    printf("Contents of Matrix:\n");
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            printf("%d ", array[i][j]);
        }
        printf("\n");
    }
}*/

void arrayMethod();
void flatMethod();
void matrixMethod();

int main(void){

	 arrayMethod();
	 matrixMethod();
	 flatMethod();


    return 0;
}

// Test multiply using a flattened 1D array
void flatMethod(){
	printf("Running test using flattened 1D arrays\n");
	size_t size = N*N;
	int* A;
	int* B;
	int* C;
	A = (int*)malloc(sizeof(int*) * size);
	B = (int*)malloc(sizeof(int*) * size);
	C = (int*)malloc(sizeof(int*) * size);

	srand(42);
	for(int i = 0; i < N; i++){
		A[i] = rand();
		B[i] = rand();
		C[i] = 0;
	}

	auto start = std::chrono::high_resolution_clock::now();
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			for(int k = 0; k < N; k++){
				C[i*N+j] += A[i*N + k] * B[k*N + j];
			}
		}
	}

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());

}

void matrixMethod(){
	printf("Running tests using Matrix data structure.\n");
	Matrix A, B, C;

	initMatrix(N,N, A);
	initMatrix(N,N, B);
	initMatrix(N,N, C);

	// Filling A,B with random numbers
	srand(42);
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			A.data[i][j] = rand();
			B.data[i][j] = rand();
		}
	}


	auto start = std::chrono::high_resolution_clock::now();
	matrixMultiply(A, B, C);
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());

}

void arrayMethod(){

	printf("Running test using standard arrays\n");
	int** A;
	int** B;
	int** C;
	A = (int**)malloc(sizeof(int*) * N);
	B = (int**)malloc(sizeof(int*) * N);
	C = (int**)malloc(sizeof(int*) * N);

	for(int i = 0; i < N; i++){
		A[i] = (int*)malloc(sizeof(int) * N);
		B[i] = (int*)malloc(sizeof(int) * N);
		C[i] = (int*)malloc(sizeof(int) * N);

	}
	// Filling A,B with random numbers
	srand(42);
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			A[i][j] = rand();
			B[i][j] = rand();
		}
	}

	auto start = std::chrono::high_resolution_clock::now();
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			C[i][j] = 0;
			for(int k = 0; k < N; k++){
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	// Save output in a file
	FILE* fp = fopen("results.data", "wb");
	fwrite(C, sizeof(int), N*N, fp);
	fclose(fp);


	printf("The duration is %f second.\n", diff.count());

}