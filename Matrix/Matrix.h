//
// Created by jhodg on 25/07/17.
//

#ifndef MATRIX_MATRIX_H
#define MATRIX_MATRIX_H

typedef struct{
		int width;
		int height;
		int** data;
}Matrix;

void matrixMultiply(Matrix &A, Matrix &B, Matrix &C) {

	for (int i = 0; i < A.height; i++) {
		for (int j = 0; j < A.width; j++) {
			for (int k = 0; k < A.height; k++) {
				C.data[i][j] += A.data[i][k] * B.data[k][j];
			}
		}
	}
}

void initMatrix(int width, int height, Matrix &matrix){
	matrix.width = width;
	matrix.height = height;

	matrix.data = (int**)malloc(sizeof(int*) * height);

	for(int i = 0; i < height; i++){
		matrix.data[i] = (int*)malloc(sizeof(int) * width);
	}

	// Initialise all data to 0 to start.
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
			matrix.data[i][j] = 0;
		}
	}
}



#endif //MATRIX_MATRIX_H
