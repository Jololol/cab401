#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <assert.h>
#include <chrono>

#define N 1000
#define NUM_THREADS	4
#define ElementType double


ElementType **A, **B, **C;

ElementType** AllocateMatrix(int n, int m, bool initialize)
{
	ElementType **matrix = (ElementType**)malloc(sizeof(ElementType*) * n);
	for (int i = 0; i < n; i++)
	{
		matrix[i] = (ElementType*)malloc(sizeof(ElementType) * m);
		if (initialize)
			for (int j = 0; j < m; j++)
				matrix[i][j] = rand();
	}

	return matrix;
}

void* matrixMultiply(void *arg){
	int threadId = *((int*) arg);
	int CHUNK_SIZE = N / NUM_THREADS;
	int start = threadId * CHUNK_SIZE;
	int finish = (int)fmin(start + CHUNK_SIZE, N);
	
	printf("thread %d %d -> %d\n", threadId, start, finish);

	for (int i = start; i < finish; i++)
		for (int j = 0; j < N; j++)
		{
			C[i][j] = 0;
			for (int k = 0; k < N; k++)
				C[i][j] += A[i][k] * B[k][j];
		}
	return 0;
}

int main(int argc, char* argv[])
{
	// ToDo:
	// 1) Vary the number of threads (how many is best)?
	// 2) Try parallelizing the j and k loop instead (do they produce the same results)?
	// 3) Try parallelizing with Pthreads library instead.

	// seed the random number generator with a constant so that we get identical/repeatable results each time we run.
	srand(42);

	A = AllocateMatrix(N, N, true);
	B = AllocateMatrix(N, N, true);
	C = AllocateMatrix(N, N, false);

	// Creatiny array of pthreads
	pthread_t* threads = (pthread_t*)malloc(sizeof(pthread_t)*NUM_THREADS);

	// time how long it takes ...
	auto start_time = std::chrono::high_resolution_clock::now();


	for(int i = 0; i < NUM_THREADS; i++){
    int *arg = (int *) malloc(sizeof(*arg));
    *arg = i;
    int thread_err = pthread_create(&(threads[i]), NULL, matrixMultiply, (void*) arg);

    if (thread_err != 0)
    	fprintf(stderr, "\nCan't create thread :[%d]", thread_err);
	}

	for(int i = 0; i < NUM_THREADS; i++){
    pthread_join(threads[i], NULL);
	}

	auto finish_time = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration = finish_time - start_time;
	printf("Duration: %f seconds\n", duration.count());

	// write the result matrix to a output file in binary format
	
	ElementType **sequential = AllocateMatrix(N, N, false);
	FILE *input_file = fopen("sequential.data", "rb");
	for (int i = 0; i < N; i++)
	{
		fread(sequential[i], sizeof(ElementType), N, input_file);
		for (int j = 0; j < N; j++)
			assert(sequential[i][j] == C[i][j]);
	}
	fclose(input_file);

	return 0;
}