
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/cudaarithm.hpp"

#include <iostream>
#include <limits.h>
#include <cstdlib>
#include <chrono>
#include <stdio.h>

using namespace std;
using namespace cv;

void printCSV(cv::Mat input);

// This is a modified version of testSequence which uses the chrono
// library to perform timing operations. These results are used to simply
// give timing benchmarks to compare against the sequential version.
void testTiming();

// This function is used to run over the sample of 520 images. This 
// function was used for performing profiling. Without any of the
// overhead of making timing calls as in testTiming()
void testSequence();

// This function is used to process a sequence of 10, 10x10 images
// The result of each processFrame() call is saved into a CSV file.
// The results of which could be compared against a similarly generated
// csv file from the sequential algorithm.
void testNumbers();

void testing();

__global__ void addGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right);
__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
						  cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
						  cv::cuda::PtrStepSzf output);

cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat alpha_prev[4]);

__device__ int getGlobalIdx_2D_2D()
{
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
	return threadId;
}



__global__ void maxTrans(cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output, int r, int size = 2)
{
	// Variables for indexing the GpuMat (Passed as PtrStepSz)

	int imgSize = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	float Beta = 0.75;
	int Col, Row;
	float max = 0.0;
	int xFactor, yFactor;


	// Check r is within specified range
	if (r < 0 || r > 3) {
		return;
	}

	switch (r) {
	case 0: // This it the Upper-Right case
		xFactor = 1;
		yFactor = -1;
		break;
	case 1: // This is the Upper-Left case
		xFactor = -1;
		yFactor = -1;
		break;
	case 2: // This is the Lower-Left case
		xFactor = -1;
		yFactor = 1;
		break;
	case 3: // This is the Lower-Right case
		xFactor = 1;
		yFactor = 1;
		break;
	}


	for (int idx = globalIdx; idx < imgSize; idx += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = idx % input.cols;
		int y = idx / input.cols;
		for (int i = 0; i < size; i++) {
			Row = y + (i * yFactor); // Row in GpuMat to access
			for (int j = 0; j < size; j++) {
				Col = x + (j * xFactor); // Column in GpuMat to access
				if ((Col < input.cols) && (Col >= 0) && (Row >= 0) && (Row < input.rows)) {
					float current = input(Row, Col);
					if (current > max) {
						max = current; // Update max value
					}
				}
			}
		}

		output(y, x) = Beta * max;

	}

}



int main(int argc, char** argv)
{
	

	testTiming();
	//testSequence();

	return 0;
}

void testTiming() {
	int counter = 0;
	string filename = "Morph" + to_string(counter) + ".bmp";
	Mat input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
	input.convertTo(input, CV_32FC1);

	cv::Mat output;
	cv::Mat empty(input.rows, input.cols, input.type(), cv::Scalar::all(0));
	alpha_prev[0].upload(empty);
	for (int r = 1; r < 4; r++) {
		alpha_prev[r] = alpha_prev[0].clone();
	}
	float total = 0;
	// Loop  through sequence of images
	for (int i = 0; i < 519; i++) {
		// Get new filename
		filename = "Morph" + to_string(counter) + ".bmp";
		// Load in image
		input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
		// Convert to 32 bit floating point
		input.convertTo(input, CV_32FC1);
		// Process output
		auto start = std::chrono::high_resolution_clock::now();
		output = processFrame(input, alpha_prev);
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> diff = end - start;
		total += diff.count();
		counter++;
	}
	cout << "Total time spent processing frames: " << total << " seconds" << std::endl;
	cout << "Average time to processFrame was: " << total / counter << " seconds." << std::endl;
}

void testSequence() {
	int counter = 0;
	string filename = "Morph" + to_string(counter) + ".bmp";
	Mat input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
	input.convertTo(input, CV_32FC1);

	cv::Mat output;
	cv::Mat empty(input.rows, input.cols, input.type(), cv::Scalar::all(0));
	cv::cuda::GpuMat alpha_prev[4];
	alpha_prev[0].upload(empty);
	for (int r = 1; r < 4; r++) {
		alpha_prev[r] = alpha_prev[0].clone();
	}
	float total = 0;
	// Loop  through sequence of images
	for (int i = 0; i < 519; i++) {
		// Get new filename
		filename = "Morph" + to_string(counter) + ".bmp";
		// Load in image
		input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
		// Convert to 32 bit floating point
		input.convertTo(input, CV_32FC1);
		// Process output
		output = processFrame(input, alpha_prev);
		
		counter++;
	}

}


void testNumbers()
{
	int n = 10;
	cv::Mat image(n, n, CV_32FC1, cv::Scalar::all(0));
	cv::cuda::GpuMat alpha_prev[4];

	for (int r = 0; r < 4; r++) {
		alpha_prev[r].upload(image);
	}

	for (int t = 0; t < n; t++) {
		//cout << "Test number : " << t + 1 << endl;
		image.at<float>(t, t) = 1;
		// Block of does processes frames and times it.
		cv::Mat output = processFrame(image, alpha_prev);
		printCSV(output);
		image.at<float>(t, t) = 0;
	}
	
}


cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat alpha_prev[4])
{
	float Beta = 0.75;
	// Variables for calling CUDA Kernel
	dim3 threadsPerBlock(8, 8);
	dim3 numBlocks(	(int)(std::ceil(input.cols / (double)threadsPerBlock.x)),
					(int)(std::ceil(input.rows / (double) threadsPerBlock.y)));

	// Stores the scaled input values
	cv::cuda::GpuMat scaledInput;
	scaledInput.upload(input);
	scaledInput.convertTo(scaledInput, scaledInput.type(), (1 - Beta));
	// Temporary dst for maxTrans output
	cv::cuda::GpuMat dst(input.rows, input.cols, input.type());
	for (int r = 0; r < 4; r++) {
		// Finding maxTrans for branch r
		maxTrans <<< numBlocks, threadsPerBlock >> >(alpha_prev[r], dst, r);	
		cudaDeviceSynchronize(); // Wait for device to complete previous kernel call

		// Add results from
		addGpuMat <<< numBlocks, threadsPerBlock >>>(dst, scaledInput);
		cudaDeviceSynchronize();
		alpha_prev[r] = dst.clone();
	}

	// GpuMat to store final output after branch combination
	cv::cuda::GpuMat gpuOut(input.rows, input.cols, input.type());
	// Branch combination step
	maxBranch << < numBlocks, threadsPerBlock >> >(alpha_prev[0], alpha_prev[1], alpha_prev[2], alpha_prev[3], gpuOut);
	cudaDeviceSynchronize();
	
	cv::Mat output;
	gpuOut.download(output); // Copy branch combination to Mat
	return output;
}

__global__ void addGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right)
{
	int size = left.rows * left.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % left.cols;
		int y = i / left.cols;
		if (x <= left.cols - 1 && y <= left.rows - 1 && y >= 0 && x >= 0) {
			left(y, x) = left(y, x) + right(y, x);
		}

	}

}

__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
	cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
	cv::cuda::PtrStepSzf output)
{
	
	int size = A.rows * A.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	float max = 0;
	
	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % A.cols;
		int y = i / A.cols;
		max = A(y, x);
		if (max < B(y, x)) {
			max = B(y, x);
		}
		if (max < C(y, x)) {
			max = C(y, x);
		}
		if (max < D(y, x)) {
			max = D(y, x);
		}
		output(y, x) = max;
	
	}

	return;
}

void printCSV(cv::Mat input)
{
	std::ofstream CSV_Output;
	CSV_Output.open("Cuda_Output.csv", std::ofstream::app);;
	CSV_Output << cv::format(input, 2);
	CSV_Output.close();
}