#ifndef VITERBI_H
#define VITERBI_H

#include <iostream>
#include <opencv2/core/core.hpp>

class Viterbi
{
    public:
        Viterbi(cv::Mat first);
        virtual ~Viterbi();

        cv::Mat processFrame(cv::Mat input);

    private:
        float maxTransition(int i, int j, int r);
        float Beta = 0.75;  // Beta forgetting factor

        int width;  // Width of frames being processed
        int height;  // Height of frames being processed

        cv::Mat alpha_prev[4];  // Remembers the previous alpha values for each branch
};

#endif // VITERBI_H
