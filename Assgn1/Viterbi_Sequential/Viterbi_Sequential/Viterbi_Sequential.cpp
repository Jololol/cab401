// Viterbi_Sequential.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <chrono>
#include "Viterbi.h"

void testing();

using namespace cv;
using namespace std;

int main()
{
	auto start = std::chrono::high_resolution_clock::now();
	testing();
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;
	cout << "Total time spent in program was: " << diff.count() << " seconds" << std::endl;
    return 0;
}


void testing() {
	int counter = 0;
	string filename = "Morph" + to_string(counter) + ".bmp";
	Mat input = imread(filename, IMREAD_UNCHANGED);
	input.convertTo(input, CV_32FC1);
	
	Mat output;

	Viterbi viterbi(input); // Initialise viterbi class.
	float total = 0;
	// Loop  through sequence of images
	for (int i = 0; i < 519; i++) {
		// Get new filename
		filename = "Morph" + to_string(counter) + ".bmp";
		// Load in image
		input = imread(filename, IMREAD_UNCHANGED);
		// Convert to 32 bit floating point
		input.convertTo(input, CV_32FC1);
		// Process output
		auto start = std::chrono::high_resolution_clock::now();
		output = viterbi.processFrame(input);
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> diff = end - start;
		total += diff.count();
		counter++;
	}
	cout << "Total time spent processing frames: " << total << " seconds" << std::endl;
	cout << "Average time to processFrame was: " << total / counter << " seconds." << std::endl;
}



