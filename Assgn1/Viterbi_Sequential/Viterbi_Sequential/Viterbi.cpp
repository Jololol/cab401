#include "viterbi.h"

Viterbi::Viterbi(cv::Mat input)
{
	// Initialise the alpha_prev arrays
	for (int i = 0; i < 4; i++) {
		;
		alpha_prev[i] = cv::Mat(input.rows, input.cols, CV_32FC1, cv::Scalar::all(0));
	}

	width = input.cols;
	height = input.rows;
}

Viterbi::~Viterbi()
{

}

cv::Mat Viterbi::processFrame(cv::Mat input) {
	// Scale input values by 1 - Beta
	input = (1 - Beta) * input;
	cv::Mat temp; // Temp mat object for each r loop

	for (int r = 0; r < 4; r++) {
		temp = input; // Initalise to scaled input values
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				// Find the maximum value for pixel(i,j) in branch r
				float maxTrans = maxTransition(i, j, r);  
				// Add maxTrans value back to temp
				temp.at<float>(i, j) = temp.at<float>(i, j) + maxTrans;
			}
		}
		//  Alpha_prev has been used, we can update it for this branch
		alpha_prev[r] = temp; 
	}

	// Output mat
	cv::Mat output(input.rows, input.cols, CV_32FC1, cv::Scalar::all(0));

	// Find max value across each branch at each pixel location and store this in
	// the final output.
	for (int j = 0; j < input.cols; j++) {
		for (int i = 0; i < input.rows; i++) {
			float maxBranch = 0; // Used to store maxValue
			for (int r = 0; r < 4; r++) {
				// Use alpha_prev as this is now the updated version for frame k
				if (alpha_prev[r].at<float>(i, j) > maxBranch) {
					maxBranch = alpha_prev[r].at<float>(i, j);
				}	
			}
			output.at<float>(i, j) = maxBranch;
		}
	}

	return output;
}

float Viterbi::maxTransition(int i, int j, int r) {
	float results[4] = { 0, 0, 0, 0 }; // Initalise the four results to 0s
	// These variables control whether the function
	// looks up/down or left/right at neighbouring pixels
	int xFactor, yFactor;
	
	switch (r) {
	case 0: // This it the Upper-Right case
		xFactor = 1;
		yFactor = -1;
		break;
	case 1: // This is the Upper-Left case
		xFactor = -1;
		yFactor = -1;
		break;
	case 2: // This is the Lower-Left case
		xFactor = -1;
		yFactor = 1;
		break;
	case 3: // This is the Lower-Right case
		xFactor = 1;
		yFactor = 1;
		break;
	}

	for (int x = 0; x < 2; x++) {
		int Col = j + x * xFactor; 
		for (int y = 0; y < 2; y++) {
			int Row = i + y * yFactor;
			if (Row >= 0 && Col >= 0 && Row < height && Col < width) {
				results[x * 2 + y] = alpha_prev[r].at<float>(Row, Col);
			}
		}
	}
	// Finding the maxValue in array
	float maxValue = 0;
	for (int i = 0; i < 4; i++) {
		if (results[i] > maxValue) {
			maxValue = results[i];
		}
	}
	// Multipy maxValue by Beta as per algorithm
	return Beta*maxValue;
}
