#include <iostream>
#include <limits.h>
#include <cstdlib>
#include <assert.h>

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"

#include "opencv2/highgui.hpp"

#include "opencv2/cudaarithm.hpp"
#include <chrono>

using namespace std;

void printCSV(cv::Mat input);
void testNumbers(int r); // Simple test using number based array
void testImage(int r); // Simple test using images

void testSequence();

__global__ void addGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right);
__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
                          cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
                          cv::cuda::PtrStepSzf output);

cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat alpha_prev[4]);

#define IMAGE_SIZE 5


__device__ int getGlobalIdx_2D_2D()
{
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
	return threadId;
}

__global__ void fillMat(cv::cuda::PtrStepSzf input)
{
	int size = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % input.cols;
		int y = i / input.rows;
		if (x <= input.cols - 1 && y <= input.rows - 1 && y >= 0 && x >= 0) {
			input(y, x) = ((y * input.cols) + x) / (float)size;
		}

	}

}


__global__ void maxTrans(cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output, int r)
{
	// Variables for indexing the GpuMat (Passed as PtrStepSz)

	int imgSize = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	int size = 2; // Size of transitional values patch
	int Col, Row;
	float max = 0.0;
	int xFactor, yFactor;


	// Check r is within specified range
	if (r < 0 || r > 3) {
		return;
	}

	switch (r) {
	case 0 : // This it the Upper-Right case
		xFactor = 1;
		yFactor = -1;
		break;
	case 1 : // This is the Upper-Left case
		xFactor = -1;
		yFactor = -1;
		break;
	case 2 : // This is the Lower-Left case
		xFactor = -1;
		yFactor = 1;
		break;
	case 3 : // This is the Lower-Right case
		xFactor = 1;
		yFactor = 1;
		break;
	}



	for (int idx = globalIdx; idx < imgSize; idx += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = idx % input.cols;
		int y = idx / input.cols;
		for (int i = 0; i < size; i++) {
			Row = y + (i * yFactor); // Row in GpuMat to access
			for (int j = 0; j < size; j++) {
				Col = x + (j * xFactor); // Column in GpuMat to access
				if ((Col < input.cols) && (Col >= 0) && (Row >= 0) && (Row < input.rows)) {
					float current = input(Row, Col);
					if (current > max) {
						max = current; // Update max value
					}
				}
			}
		}

		output(y, x) = max;

	}

}

int main(int argc, char** argv)
{
	int r;
	if (argv[1]) {
		r = atoi(argv[1]);
	} else {
		r = 0;
	}


	//testNumbers(r);
	//testImage(r);
	//testMultiply();

	testSequence();


}

void testSequence()
{
	int n = 1500;
	int height = n;
	int width = n;
	cv::Mat image(width, height, CV_32FC1, cv::Scalar::all(0));
	cv::cuda::GpuMat alpha_prev[4];
	for (int r = 0; r < 4; r++) {
		alpha_prev[r].upload(image);
	}


	cv::namedWindow("testing", cv::WINDOW_NORMAL);

	for (int t = 0; t < n; t++) {
		int x = t;
		int y = t;

		//cout << "Test number : " << t + 1 << endl;
		image.at<float>(y, x) = 1;
		imshow("input", image);

		// Block of does processes frames and times it.
		cv::Mat output = processFrame(image, alpha_prev);


		//imshow("testing", output);
		
		image.at<float>(y, x) = 0;
		
		
		//cv::waitKey(3);
	}


}

void testNumbers(int r)
{

	cv::Mat input(IMAGE_SIZE, IMAGE_SIZE, CV_32FC1);

	std::cout << "Got here!" << std::endl;

	cv::cuda::GpuMat src;
	src.upload(input);

	dim3 threadsPerBlock(32, 32);
	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));

	std::cout << "Accessing kernel with following threads/blocks." << std::endl;
	std::cout << "Threads(x,y): (" << threadsPerBlock.x << "," << threadsPerBlock.y << ")" << std::endl;
	std::cout << "Blocks(x,y): (" << numBlocks.x << "," << numBlocks.y << ")" << std::endl;
	fillMat <<< numBlocks, threadsPerBlock>>>(src);
	cudaDeviceSynchronize();


	src.download(input);
	cout << "Original matrix input: " << endl;
	cout << input << endl << endl;

	imshow("Output", input);
	cv::waitKey(0);

	cv::cuda::GpuMat dst(src.rows, src.cols, src.type());

	auto start = std::chrono::high_resolution_clock::now();

	// Perform maxTrans
	maxTrans <<< numBlocks, threadsPerBlock>>>(src, dst, r);
	cudaDeviceSynchronize();

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());


	dst.download(input);
	cout << input << endl << endl;

	imshow("Output", input);
	cv::waitKey(0);
}

void testImage(int r)
{


	//Load in test image
	cv::Mat input = cv::imread("test.png", 2);

	// Conver to floating point
	input.convertTo(input, CV_32FC1, (1.0 / 255.0));
	imshow("input after conversion to 32F", input);
	cv::waitKey(0);


	if (input.empty()) {
		cout << "Could not open or find image" << endl;
		return;
	}


	cv::cuda::GpuMat src;
	src.upload(input);

	dim3 threadsPerBlock(8, 8);
	std::cout << input.size().width << std::endl;
	std::cout << input.size().height << std::endl;

	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));


	std::cout << "Accessing kernel with following threads/blocks." << std::endl;
	std::cout << "Threads(x,y): (" << threadsPerBlock.x << "," << threadsPerBlock.y << ")" << std::endl;
	std::cout << "Blocks(x,y): (" << numBlocks.x << "," << numBlocks.y << ")" << std::endl;

	std::cout << "Image size is: " << input.rows * input.cols << std::endl;
	std::cout << "Threads size is: " << threadsPerBlock.x * threadsPerBlock.y * numBlocks.x * numBlocks.y << std::endl;

	src.download(input);
	imshow("input", input);

	cv::cuda::GpuMat dst(src.rows, src.cols, src.type());

	auto start = std::chrono::high_resolution_clock::now();

	maxTrans <<< numBlocks, threadsPerBlock>>>(src, dst, r);

	cudaDeviceSynchronize();

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());


	dst.download(input);

	imshow("Output", input);
	cv::waitKey(0);
}

cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat alpha_prev[4])
{
	float Beta = 0.75;

	// Variables for calling CUDA Kernel
	dim3 threadsPerBlock(8, 8);
	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));


	std::vector<cv::cuda::GpuMat> alpha_r(4);;
	for (int r = 0; r < 4; r++) {
		alpha_r.push_back(cv::cuda::GpuMat(input.cols, input.rows, CV_32FC1, cv::Scalar::all(0)));
		alpha_r[r].upload(input);
	}

	for (int r = 0; r < 4; r++) {
		alpha_r[r].convertTo(alpha_r[r], alpha_r[r].type(), (1 - Beta));
		cv::cuda::GpuMat dst(input.rows, input.cols, input.type()); // Temporary dst for maxTrans output

		// Finding maxTrans for specific r
		maxTrans <<< numBlocks, threadsPerBlock>>>(alpha_prev[r], dst, r);
		cudaDeviceSynchronize(); // Wait for device to complete previous kernel call


		// Multiply maxTrans value by Beta
		dst.convertTo(dst, dst.type(), Beta);

		// Add results from

		addGpuMat <<< numBlocks, threadsPerBlock>>>(alpha_r[r], dst);
		cudaDeviceSynchronize();



		alpha_prev[r] = alpha_r[r].clone();

	}

	cv::cuda::GpuMat gpuOut(input.rows, input.cols, input.type());


	maxBranch <<< numBlocks, threadsPerBlock>>>(alpha_r[0], alpha_r[1], alpha_r[2], alpha_r[3], gpuOut);
	cudaDeviceSynchronize();


	cv::Mat output;

	gpuOut.download(output);

	return output;
}

__global__ void addGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right)
{
	int size = left.rows * left.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % left.cols;
		int y = i / left.cols;
		if (x <= left.cols - 1 && y <= left.rows - 1 && y >= 0 && x >= 0) {
			left(y, x) = left(y, x) + right(y, x);
		}

	}

}

__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
                          cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
                          cv::cuda::PtrStepSzf output)
{

	int size = A.rows * B.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	float max;

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % A.cols;
		int y = i / A.rows;
		max = A(y, x);
		if (max < B(y, x)) {
			max = B(y, x);
		}
		if (max < C(y, x)) {
			max = C(y, x);
		}
		if (max < D(y, x)) {
			max = D(y, x);
		}
		output(y, x) = max;
	}

}

void printCSV(cv::Mat input)
{
	std::ofstream CSV_Output;
	CSV_Output.open("Cuda_Output.csv", std::ofstream::app);;
	CSV_Output << cv::format(input, 2);
	CSV_Output.close();
}




